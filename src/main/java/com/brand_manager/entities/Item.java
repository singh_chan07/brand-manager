package com.brand_manager.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    private long brandId;

    @PrePersist
    public void setCreationDate(){
        this.createdOn = new Date();
    }

    @PreUpdate
    public void setChangeDate(){
        this.updatedOn = new Date();
    }


}
