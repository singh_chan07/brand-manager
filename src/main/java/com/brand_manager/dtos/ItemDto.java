package com.brand_manager.dtos;

import lombok.Data;

@Data
public class ItemDto {

    private long id;

    private String name;

    private long brandId;
}
