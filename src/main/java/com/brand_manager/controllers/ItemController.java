package com.brand_manager.controllers;


import com.brand_manager.dtos.ItemDto;
import com.brand_manager.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    ItemService itemService;

    @GetMapping("/{brandId}")
    public ResponseEntity<List<ItemDto>> getItems(@PathVariable long brandId){
        return new ResponseEntity<>(itemService.getAll(brandId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ItemDto> add(@RequestBody ItemDto itemDto){
        return new ResponseEntity<>(itemService.add(itemDto), HttpStatus.OK);
    }

    @DeleteMapping("/{itemId}")
    public ResponseEntity<Object> delete(@PathVariable long itemId){
        itemService.delete(itemId);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
