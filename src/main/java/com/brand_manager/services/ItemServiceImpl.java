package com.brand_manager.services;

import com.brand_manager.dtos.ItemDto;
import com.brand_manager.entities.Item;
import com.brand_manager.repositories.ItemRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class ItemServiceImpl implements ItemService{

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public ItemDto add(ItemDto itemDto) {
        Item item = modelMapper.map(itemDto, Item.class);
        return modelMapper.map(itemRepository.save(item), ItemDto.class);
    }

    @Override
    public List<ItemDto> getAll(long brandId) {
        return itemRepository.findByBrandId(brandId).get()
                .stream().map(i -> modelMapper.map(i, ItemDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(long itemId) {
        itemRepository.deleteById(itemId);
    }
}
