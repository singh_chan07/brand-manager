package com.brand_manager.services;

import com.brand_manager.dtos.ItemDto;

import java.util.List;

public interface ItemService {

    ItemDto add(ItemDto itemDto);
    List<ItemDto> getAll(long brandId);
    void delete(long itemId);
}
